package com.floorwatch.rest;

import com.firebase.security.token.TokenGenerator;
import com.floorwatch.common.converters.UserConverter;
import com.floorwatch.common.pojos.SimpleUser;
import com.floorwatch.common.pojos.SimpleUserStore;
import com.floorwatch.common.utils.UserUtilities;
import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UserStoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import com.qmino.miredot.annotations.ReturnType;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import org.apache.log4j.Logger;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Authentication Resource
 */

@Path("/authentication")
public class AuthenticationResource {

    private static final Logger log = Logger.getLogger(AuthenticationResource.class);

    public AuthenticationResource() {
    }
    
    /**
     * Java Client Example<br/><br/>
     * SimpleUser user = new SimpleUser();<br/>
     * user.setUserName("gmirabito");<br/>
     * user.setPassword("4court");<br/>
     * ClientConfig clientConfig = new DefaultClientConfig();<br/>
     * clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);<br/>
     * Client client = Client.create(clientConfig);<br/>
     * // Localhost<br/>
     * WebResource webResource = client.resource("http://localhost:8080/rest/authentication");<br/>
     * ClientResponse response = webResource.type("application/json").put(ClientResponse.class, user);<br/>
     * if (response.getStatus() != 200) {<br/>
     *     throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());<br/>
     * }<br/> 
     * 
     * @summary Authenticates a manager
     * 
     * @param   simpleUser The HTTP request.
     * 
     * @author  Dale Davis
     * @date    12/9/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 403 - Invalid username or password.
     * @statuscode 500 - Database error.
     */    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("com.floorwatch.common.pojos.SimpleUser<T>")
    public Response put(SimpleUser simpleUser) {
        ObjectMapper mapper = new ObjectMapper();
        UsersFacade uf = new UsersFacade();
        Users manager = null;
        String json = null;
        try {
            manager = uf.findByUsername(simpleUser.getUserName());
        } catch (NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid username or password."), Response.Status.BAD_REQUEST);
        } catch (Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        boolean authenticated = UserUtilities.authenticate(manager, simpleUser.getPassword());
        // If authentication fails, return an error
        if (!authenticated) {
            throw new WebApplicationException(new Throwable("Invalid username or password."), Response.Status.BAD_REQUEST);
        }
        simpleUser = UserConverter.toPojo(manager);
        // Get a firebase token
        Map<String, Object> authPayload = new HashMap<String, Object>();
        authPayload.put("uid", String.valueOf(simpleUser.getId()));
        authPayload.put("name", simpleUser.getFirstName() + " " + simpleUser.getLastName());
        TokenGenerator tokenGenerator = new TokenGenerator("tg5I4XHFNUlxUltUSNxEVHfFQdZG1Boayw7e2DPa");
        String token = null;
        try {
            token = tokenGenerator.createToken(authPayload);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Unable to generate firebase token."), Response.Status.BAD_REQUEST);
        }
        simpleUser.setFirebaseToken(token);
        try {
            json = mapper.writeValueAsString(simpleUser);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }    
        return Response.ok(json).build();    
    }    
    
}

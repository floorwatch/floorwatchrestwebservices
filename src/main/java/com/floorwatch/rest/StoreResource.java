package com.floorwatch.rest;

import com.floorwatch.common.converters.StoreConverter;
import com.floorwatch.common.pojos.SimpleStore;
import com.floorwatch.entities.Stores;
import com.floorwatch.entities.manager.StoresFacade;
import com.qmino.miredot.annotations.ReturnType;
import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Store Resource
 */

@Path("/store")
public class StoreResource {

    private static final Logger log = Logger.getLogger(StoreResource.class);

    public StoreResource() {
    }
    
    /**
     * @summary Returns all stores given a city and state
     * 
     * @param   city    The city.
     * @param   state   The state.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 500 - Database error.
     */    
    
    @GET
    @Path("/city/{city: .+}/state/{state: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("java.util.List<com.floorwatch.common.pojos.SimpleStore>")
    public Response getByCity(@PathParam("city") String city, @PathParam("state") String state) {
        ObjectMapper mapper = new ObjectMapper();
        StoresFacade sf = new StoresFacade();
        List<Stores> nearestStores = new ArrayList<>();
        String json = null;
        try {
            nearestStores = sf.findByCity(city, state);
            List<SimpleStore> simpleStores = new ArrayList<>();
            for (Stores store : nearestStores) {
                SimpleStore simpleStore = StoreConverter.toPojo(store);
                simpleStores.add(simpleStore);
            }
            json = mapper.writeValueAsString(simpleStores);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();

    }    

    /**
     * @summary Returns all stores in a radius given a latitude, longitude and distance
     * 
     * @param   lat         The latitude.
     * @param   lon         The longitude.
     * @param   distance    The radius distance.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 500 - Database error.
     */    
    
    @GET
    @Path("/nearest/{lat: .+}/{lon: .+}/{distance: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("java.util.List<com.floorwatch.common.pojos.SimpleStore>")
    public Response getNearest(@PathParam("lat") Double lat, @PathParam("lon") Double lon, @PathParam("distance") Double distance) {
        ObjectMapper mapper = new ObjectMapper();
        StoresFacade sf = new StoresFacade();
        List<Stores> nearestStores = new ArrayList<>();
        String json = null;
        try {
            nearestStores = sf.findNearest(lat, lon, distance);
            List<SimpleStore> simpleStores = new ArrayList<>();
            for (Stores store : nearestStores) {
                SimpleStore simpleStore = StoreConverter.toPojo(store);
                simpleStores.add(simpleStore);
            }
            json = mapper.writeValueAsString(simpleStores);
        } catch(Exception e) {
            
        }
        return Response.ok(json).build();

    }
}

package com.floorwatch.rest;

import com.floorwatch.common.converters.UserConverter;
import com.floorwatch.common.pojos.SimpleUser;
import com.floorwatch.common.pojos.SimpleUserStore;
import com.floorwatch.common.utils.UserUtilities;
import com.floorwatch.entities.UserStores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UserStoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import com.qmino.miredot.annotations.ReturnType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import org.apache.log4j.Logger;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Manager Resource
 */

@Path("/manager")
public class ManagerResource {

    private static final Logger log = Logger.getLogger(ManagerResource.class);

    public ManagerResource() {
    }
    
    /**
     * @summary Returns on-duty manager given a store id
     * 
     * @param   storeId   The store id.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 500 - Database error.
     */    
    
    @GET
    @Path("/store/{storeId: .+}/onduty")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("java.util.List<com.floorwatch.common.pojos.SimpleUser>")
    public Response getOnDutyManagers(@PathParam("storeId") Integer storeId) {
        ObjectMapper mapper = new ObjectMapper();
        UsersFacade uf = new UsersFacade();
        List<Users> onDutyManagers = new ArrayList<>();
        String json = null;
        try {
            onDutyManagers = uf.findOnDutyByStore(storeId);
            List<SimpleUser> simpleUsers = new ArrayList<>();
            for (Users manager : onDutyManagers) {
                SimpleUser simpleUser = UserConverter.toPojo(manager);
                simpleUsers.add(simpleUser);
            }
            json = mapper.writeValueAsString(simpleUsers);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();
    }
    
    /**
     * @summary Returns manager given a username
     * 
     * @param   username   The username.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 403 - Invalid username.
     * @statuscode 500 - Database error.
     */    
    
    @GET
    @Path("/username/{username: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("com.floorwatch.common.pojos.SimpleUser<T>")
    public Response getManagerByUsername(@PathParam("username") String username) {
        ObjectMapper mapper = new ObjectMapper();
        UsersFacade uf = new UsersFacade();
        Users manager = null;
        String json = null;
        try {
            manager = uf.findByUsername(username);
            SimpleUser simpleUser = UserConverter.toPojo(manager);
            json = mapper.writeValueAsString(simpleUser);
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Manager not found."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();
    }    
    
    /**
     * Java Client Example<br/><br/>
     * SimpleUserStore userStore = new SimpleUserStore();<br/>
     * SimpleUser user = new SimpleUser();<br/>
     * SimpleStore store = new SimpleStore();<br/>
     * user.setId(5);<br/>
     * userStore.setUser(user);<br/>
     * store.setId(1);<br/>
     * userStore.setStore(store);<br/>
     * userStore.setOnDuty(true);<br/>
     * ClientConfig clientConfig = new DefaultClientConfig();<br/>
     * clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);<br/>
     * Client client = Client.create(clientConfig);<br/>
     * // Localhost<br/>
     * WebResource webResource = client.resource("http://localhost:8080/rest/manager");<br/>
     * ClientResponse response = webResource.type("application/json").put(ClientResponse.class, userStore);<br/>
     * if (response.getStatus() != 204) {<br/>
     *      throw new RuntimeException("Failed : HTTP error code : response.getStatus());<br/>
     * }<br/>
     * 
     * @summary Updates a manager's status
     * 
     * @param   simpleUserStore The HTTP request.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 204 - Success.
     * @statuscode 403 - Invalid flare id.
     * @statuscode 500 - Database error.
     */    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(SimpleUserStore simpleUserStore) {
        UserStoresFacade usf = new UserStoresFacade();
        UsersFacade uf = new UsersFacade();
        Users user = null;
        UserStores userStore = null;
        // Find the user
        try {
            user = uf.findById(simpleUserStore.getUser().getId());
        } catch(NoResultException nre) {
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        // If the user does not exist, return an error
        if (user == null) {
            throw new WebApplicationException(new Throwable("User not found."), Response.Status.BAD_REQUEST);
        }
        // Find the user store from the user
        for (UserStores currentUserStore : user.getUserStoresList()) {
            if (currentUserStore.getStoreId().getId().intValue() == simpleUserStore.getStore().getId()) {
                userStore = currentUserStore;
                break;
            }
        }
        userStore.setOnDuty(simpleUserStore.isOnDuty() ? new Short("1") : new Short("0"));
        userStore.setUpdatedAt(new Date());
        userStore.setUpdatedBy(1);
        // Save the new user
        try {
            userStore = usf.save(userStore);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.noContent().build();        
    }
    
}

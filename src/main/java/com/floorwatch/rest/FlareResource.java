package com.floorwatch.rest;

import com.floorwatch.common.converters.FlareConverter;
import com.floorwatch.common.pojos.SimpleFlare;
import com.floorwatch.common.utils.DateUtilities;
import com.floorwatch.entities.Flares;
import com.floorwatch.entities.Stores;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.FlaresFacade;
import com.floorwatch.entities.manager.StoresFacade;
import com.floorwatch.entities.manager.UsersFacade;
import com.qmino.miredot.annotations.ReturnType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import org.apache.log4j.Logger;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;   
    
/**
 * Flare Resource
 */

@Path("/flare")
public class FlareResource {

    private static final Logger log = Logger.getLogger(FlareResource.class);

    public FlareResource() {
    }
   
    /**
     * @summary Returns a flare given an id
     * 
     * @param   flareId   The flare id.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 403 - Invalid flare id.
     * @statuscode 500 - Database error.
     */    
    @GET
    @Path("/{flareId: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("com.floorwatch.common.pojos.SimpleFlare<T>")    
    public Response getById(@PathParam("flareId") Integer flareId) {
        ObjectMapper mapper = new ObjectMapper();
        FlaresFacade ff = new FlaresFacade();
        Flares flare = null;
        String json = null;
        try {
            flare = ff.findById(flareId);
            SimpleFlare simpleFlare = FlareConverter.toPojo(flare);
            json = mapper.writeValueAsString(simpleFlare);
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid flare."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();

    }
    
    /**
     * @summary Returns a flare given a request time and username
     * 
     * @param   requestTime     The request time in seconds since 2000.
     * @param   username        The basic user's username.
     * 
     * @author  Dale Davis
     * @date    12/30/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 403 - Invalid request time/username.
     * @statuscode 500 - Database error.
     */    
    @GET
    @Path("/time/{requestTime: .+}/user/{username: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("com.floorwatch.common.pojos.SimpleFlare<T>")    
    public Response getByTimeUser(@PathParam("requestTime") Long requestTime, @PathParam("username") String username) {
        ObjectMapper mapper = new ObjectMapper();
        FlaresFacade ff = new FlaresFacade();
        Flares flare = null;
        String json = null;
        // Convert seconds since 2000 to Java Date object
        long millis = DateUtilities.convertSecsSince2000ToMillisSince1970(requestTime);
        Date createdAt = new Date(millis);
        try {
            flare = ff.findByTimeUser(createdAt, username);
            SimpleFlare simpleFlare = FlareConverter.toPojo(flare);
            json = mapper.writeValueAsString(simpleFlare);
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid flare."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();

    }
    
    /**
     * @summary Returns current unresolved flares given a store id
     * 
     * @param   storeId   The store id.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 500 - Database error.
     */    
    @GET
    @Path("/store/{storeId: .+}/current")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("java.util.List<com.floorwatch.common.pojos.SimpleFlare>")
    public Response getCurrentUnresolved(@PathParam("storeId") Integer storeId) {
        ObjectMapper mapper = new ObjectMapper();
        FlaresFacade ff = new FlaresFacade();
        List<Flares> currentFlares = new ArrayList<>();
        String json = null;
        try {
            currentFlares = ff.findActiveUnresolvedByStore(storeId);
            List<SimpleFlare> simpleFlares = new ArrayList<>();
            for (Flares flare : currentFlares) {
                SimpleFlare simpleFlare = FlareConverter.toPojo(flare);
                simpleFlares.add(simpleFlare);
            }
            json = mapper.writeValueAsString(simpleFlares);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();

    }
    
    /**
     * Java Client Example<br/><br/>
     * SimpleFlare flare = new SimpleFlare();<br/>
     * SimpleUser customerUser = new SimpleUser();<br/>
     * customerUser.setId(7);<br/>
     * flare.setCustomerUser(customerUser);<br/>
     * SimpleStore store = new SimpleStore();<br/>
     * store.setId(1);<br/>
     * flare.setStore(store);<br/>
     * flare.setCustomerText("I need help in Shoes.");<br/>
     * ClientConfig clientConfig = new DefaultClientConfig();<br/>
     * clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);<br/>
     * Client client = Client.create(clientConfig);<br/>
     * // Localhost<br/>
     * WebResource webResource = client.resource("http://localhost:8080/rest/flare");<br/>
     * ClientResponse response = webResource.type("application/json").post(ClientResponse.class, flare);<br/>
     * if (response.getStatus() != 201) {<br/>
     *     throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());<br/>
     * }<br/>
     * 
     * @summary Creates a new flare
     * 
     * @param   simpleFlare The HTTP request.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response with flare id.
     * 
     * @statuscode 201 - Success.
     * @statuscode 403 - Invalid user or store id.
     * @statuscode 500 - Database error.
     */    

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response post(SimpleFlare simpleFlare) {
        UsersFacade uf = new UsersFacade();
        StoresFacade sf = new StoresFacade();
        FlaresFacade ff = new FlaresFacade();
        Users dbCustomerUser = null;
        Users dbManagerUser = null;        
        Stores dbStore = null;
        
        // Get the customer user, manager user and store
        try {
            dbCustomerUser = uf.findById(simpleFlare.getCustomerUser().getId());
            dbStore = sf.findById(simpleFlare.getStore().getId());
            if (simpleFlare.getManagerUser() != null && simpleFlare.getManagerUser().getId() > 0) {
                dbManagerUser = uf.findById(simpleFlare.getManagerUser().getId());
            }            
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid user or store."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        Flares flare = new Flares();
        flare.setCustomerUserId(dbCustomerUser);
        flare.setStoreId(dbStore);
        if (dbManagerUser != null) {
            flare.setManagerUserId(dbManagerUser);
        }        
        flare.setCustomerText(simpleFlare.getCustomerText());
        if (simpleFlare.getManagerText() != null && simpleFlare.getManagerText().length() > 0) { 
            flare.setManagerText(simpleFlare.getManagerText());
        }    
        if (simpleFlare.getCustomerFollowupText() != null && simpleFlare.getCustomerFollowupText().length() > 0) { 
            flare.setCustomerFollowupText(simpleFlare.getCustomerFollowupText());
        }
        if (simpleFlare.getManagerFollowupText() != null && simpleFlare.getManagerFollowupText().length() > 0) { 
            flare.setManagerFollowupText(simpleFlare.getManagerFollowupText());
        }        
        flare.setResolved(simpleFlare.isResolved() ? new Short("1") : new Short("0"));
        if (simpleFlare.getResolvedAt() != null) {
            flare.setResolvedAt(new Date(simpleFlare.getResolvedAt()));
        }
        if (simpleFlare.getResolvedStars() != null  && simpleFlare.getResolvedStars() > 0) {
            flare.setResolvedStars(simpleFlare.getResolvedStars().shortValue());
        }
        if (simpleFlare.getResolvedText() != null && simpleFlare.getResolvedText().length() > 0) {
            flare.setResolvedText(simpleFlare.getResolvedText());
        }        
        flare.setCreatedAt(new Date());
        flare.setCreatedBy(1);
        flare.setUpdatedAt(new Date());
        flare.setUpdatedBy(1);
        // Save the new flare
        try {
            flare = ff.save(flare);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.status(201).entity(flare.getId().toString()).build();   
    }
    
    /**
     * Java Client Example<br/><br/>
     * SimpleFlare flare = new SimpleFlare();<br/>
     * flare.setId(1);<br/>
     * SimpleUser managerUser = new SimpleUser();<br/>
     * managerUser.setId(5);<br/>
     * flare.setManagerUser(managerUser);<br/>
     * flare.setManagerText("I can't find you.");<br/>
     * ClientConfig clientConfig = new DefaultClientConfig();<br/>
     * clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);<br/>
     * Client client = Client.create(clientConfig);<br/>
     * // Localhost<br/>
     * WebResource webResource = client.resource("http://localhost:8080/rest/flare");<br/>
     * ClientResponse response = webResource.type("application/json").put(ClientResponse.class, flare);<br/>
     * if (response.getStatus() != 204) {<br/>
     *     throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());<br/>
     * }<br/>
     * 
     * @summary Updates a flare
     * 
     * @param   simpleFlare The HTTP request.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response.
     * 
     * @statuscode 204 - Success.
     * @statuscode 403 - Invalid user, store, or flare id.
     * @statuscode 500 - Database error.
     */    
    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(SimpleFlare simpleFlare) {
        UsersFacade uf = new UsersFacade();
        StoresFacade sf = new StoresFacade();
        FlaresFacade ff = new FlaresFacade();
        Users dbCustomerUser = null;
        Users dbManagerUser = null;
        Flares dbFlare = null;
        Stores dbStore = null;
        
        // Get the customer user, manager user and store
        try {
            dbFlare = ff.findById(simpleFlare.getId());
            if (simpleFlare.getCustomerUser() != null && simpleFlare.getCustomerUser().getId() > 0) {
                dbCustomerUser = uf.findById(simpleFlare.getCustomerUser().getId());
            }
            if (simpleFlare.getManagerUser() != null && simpleFlare.getManagerUser().getId() > 0) {
                dbManagerUser = uf.findById(simpleFlare.getManagerUser().getId());
            }
            if (simpleFlare.getStore() != null && simpleFlare.getStore().getId() > 0) {
                dbStore = sf.findById(simpleFlare.getStore().getId());
            }
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid user, store or flare."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        if (dbCustomerUser != null) {
            dbFlare.setCustomerUserId(dbCustomerUser);
        }
        if (dbManagerUser != null) {
            dbFlare.setManagerUserId(dbManagerUser);
        }    
        if (dbStore != null) {
            dbFlare.setStoreId(dbStore);
        }    
        if (simpleFlare.getManagerText() != null && simpleFlare.getManagerText().length() > 0) { 
            dbFlare.setManagerText(simpleFlare.getManagerText());
        }    
        if (simpleFlare.getCustomerFollowupText() != null && simpleFlare.getCustomerFollowupText().length() > 0) { 
            dbFlare.setCustomerFollowupText(simpleFlare.getCustomerFollowupText());
        }
        if (simpleFlare.getManagerFollowupText() != null && simpleFlare.getManagerFollowupText().length() > 0) { 
            dbFlare.setManagerFollowupText(simpleFlare.getManagerFollowupText());
        }        
        dbFlare.setResolved(simpleFlare.isResolved() ? new Short("1") : new Short("0"));
        if (simpleFlare.getResolvedAt() != null) {
            dbFlare.setResolvedAt(new Date(simpleFlare.getResolvedAt()));
        }
        if (simpleFlare.getResolvedStars() != null  && simpleFlare.getResolvedStars() > 0) {
            dbFlare.setResolvedStars(simpleFlare.getResolvedStars().shortValue());
        }
        if (simpleFlare.getResolvedText() != null && simpleFlare.getResolvedText().length() > 0) {
            dbFlare.setResolvedText(simpleFlare.getResolvedText());
        } 
        dbFlare.setUpdatedAt(new Date());
        dbFlare.setUpdatedBy(1);
        // Save the updated flare
        try {
            ff.save(dbFlare);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.noContent().build();        
    }    
}

package com.floorwatch.rest;

import com.floorwatch.common.converters.UserConverter;
import com.floorwatch.common.pojos.SimpleNetwork;
import com.floorwatch.common.pojos.SimpleUser;
import com.floorwatch.entities.Networks;
import com.floorwatch.entities.UserNetworks;
import com.floorwatch.entities.Users;
import com.floorwatch.entities.manager.UsersFacade;
import com.qmino.miredot.annotations.ReturnType;
import java.util.Date;
import javax.persistence.NoResultException;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import org.apache.log4j.Logger;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * User Resource
 */

@Path("/networkuser")
public class NetworkUserResource {

    private static final Logger log = Logger.getLogger(NetworkUserResource.class);

    public NetworkUserResource() {
    }
    
    /**
     * @summary Returns user given a username
     * 
     * @param   username   The username.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response
     * 
     * @statuscode 200 - Success.
     * @statuscode 403 - Invalid username.
     * @statuscode 500 - Database error.
     */    

    @GET
    @Path("/{username: .+}")
    @Produces(MediaType.APPLICATION_JSON)
    @ReturnType("com.floorwatch.common.pojos.SimpleUser<T>")
    public Response getByUsername(@PathParam("username") String username) {
        ObjectMapper mapper = new ObjectMapper();
        UsersFacade uf = new UsersFacade();
        Users user = null;
        String json = null;
        try {
            user = uf.findByUsername(username);
            SimpleUser simpleUser = UserConverter.toPojo(user);
            json = mapper.writeValueAsString(simpleUser);
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("User not found."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.ok(json).build();
    }    
    
    /**
     * Java Client Example<br/><br/>
     * SimpleUser user = new SimpleUser();<br/>
     * user.setUserName("rocky.davis.5602");<br/>
     * user.setFirstName("Rocky");<br/>
     * user.setLastName("Davis");<br/>
     * user.setEmailAddress("ddavis1933@gmail.com");<br/>
     * user.setNetworks(new ArrayList());<br/>
     * SimpleNetwork network = new SimpleNetwork();<br/>
     * network.setDescription(SimpleNetwork.Network.Facebook.toString());<br/>
     * network.setUid("112855475736382");<br/>
     * // Profile pic<br/>
     * InputStream inputStream = new FileInputStream(new File("src/main/resources/rocky.jpg"));<br/>
     * byte[] bytes;<br/>
     * byte[] buffer = new byte[8192];<br/>
     * int bytesRead;<br/>
     * ByteArrayOutputStream out = new ByteArrayOutputStream();<br/>
     * try {<br/>
     *     while ((bytesRead = inputStream.read(buffer)) != -1) {<br/>
     *         out.write(buffer, 0, bytesRead);<br/>
     *     }<br/>
     * } catch (IOException e) {<br/>
     *     e.printStackTrace();<br/>
     * }<br/>
     * bytes = out.toByteArray();<br/>
     * String encodedString = Base64.encodeBase64URLSafeString(bytes);<br/>
     * network.setProfilePicture(encodedString);<br/>
     * user.getNetworks().add(network);<br/>
     * ClientConfig clientConfig = new DefaultClientConfig();<br/>
     * clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);<br/>
     * Client client = Client.create(clientConfig);<br/>
     * // Localhost<br/>
     * WebResource webResource = client.resource("http://localhost:8080/rest/networkuser");<br/>
     * ClientResponse response = webResource.type("application/json").post(ClientResponse.class, user);<br/>
     * if (response.getStatus() != 201) {<br/>
     *     throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());<br/>
     * }<br/>
     * 
     * @summary Creates a new social network user
     * 
     * @param   simpleUser The HTTP request.
     * 
     * @author  Dale Davis
     * @date    12/7/2015
     * 
     * @return  The HTTP response with user id.
     * 
     * @statuscode 201 - Success.
     * @statuscode 403 - User already exists or invalid social network id.
     * @statuscode 500 - Database error.
     */    
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response post(SimpleUser simpleUser) {
        UsersFacade uf = new UsersFacade();
        Users dbUser = null;
        // Check the username
        try {
            dbUser = uf.findByUsername(simpleUser.getUserName());
        } catch(NoResultException nre) {
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        // If the user exists return an error
        if (dbUser != null) {
            throw new WebApplicationException(new Throwable("User already exists."), Response.Status.BAD_REQUEST);
        }
        Users user = null;
        try {
            user = UserConverter.toEntity(simpleUser);
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid social network or role."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        // Misc user object info
        user.setIsActive(new Short("1"));
        user.setCreatedAt(new Date());
        user.setCreatedBy(1);
        user.setUpdatedAt(new Date());
        user.setUpdatedBy(1);
        // Save the new user
        try {
            user = uf.save(user);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.status(201).entity(user.getId().toString()).build();        
    }
    
    /**
     * Java Client Example<br/><br/>
     * SimpleUser user = new SimpleUser();<br/>
     * user.setUserName("rocky.davis.5602");<br/>
     * user.setNetworks(new ArrayList());<br/>
     * SimpleNetwork network = new SimpleNetwork();<br/>
     * network.setDescription(SimpleNetwork.Network.Facebook.toString());
     * InputStream inputStream = new FileInputStream(new File("src/main/resources/rocky2.jpg"));<br/>
     * byte[] bytes;<br/>
     * byte[] buffer = new byte[8192];<br/>
     * int bytesRead;<br/>
     * ByteArrayOutputStream out = new ByteArrayOutputStream();<br/>
     * try {<br/>
     *     while ((bytesRead = inputStream.read(buffer)) != -1) {<br/>
     *         out.write(buffer, 0, bytesRead);<br/>
     *     }<br/>
     * } catch (IOException e) {<br/>
     *     e.printStackTrace();<br/>
     * }<br/>
     * bytes = out.toByteArray();<br/>
     * String encodedString = Base64.encodeBase64URLSafeString(bytes);<br/> 
     * network.setProfilePicture(encodedString);<br/>
     * user.getNetworks().add(network);<br/>
     * ClientConfig clientConfig = new DefaultClientConfig();<br/>
     * clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);<br/>
     * Client client = Client.create(clientConfig);<br/>
     * // Localhost<br/>
     * WebResource webResource = client.resource("http://localhost:8080/rest/networkuser");<br/>
     * ClientResponse response = webResource.type("application/json").put(ClientResponse.class, user);<br/>
     * if (response.getStatus() != 204) {<br/>
     *     throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());<br/>
     * }
     * 
     * @summary Updates a social network user
     * 
     * @param   simpleUser The HTTP request.
     * 
     * @author  Dale Davis
     * @date    1/29/2016
     * 
     * @return  The HTTP response.
     * 
     * @statuscode 204 - Success.
     * @statuscode 403 - Invalid user.
     * @statuscode 500 - Database error.
     */    
    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    public Response put(SimpleUser simpleUser) {
        UsersFacade uf = new UsersFacade();
        Users dbUser = null;
        // Check the username
        try {
            dbUser = uf.findByUsername(simpleUser.getUserName());
        } catch(NoResultException nre) {
            throw new WebApplicationException(new Throwable("Invalid user."), Response.Status.BAD_REQUEST);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        
        if (simpleUser.getEmailAddress() != null && simpleUser.getEmailAddress().length() > 0) {
            dbUser.setEmailAddress(simpleUser.getEmailAddress());
        }
        if (simpleUser.getFirstName()!= null && simpleUser.getFirstName().length() > 0) {
            dbUser.setFirstName(simpleUser.getFirstName());
        }
        if (simpleUser.getLastName() != null && simpleUser.getLastName().length() > 0) {
            dbUser.setLastName(simpleUser.getLastName());
        }
        // Network changes
        for (SimpleNetwork network : simpleUser.getNetworks()) {
            UserNetworks dbUserNetwork = null;
            for (UserNetworks userNetwork : dbUser.getUserNetworksList()) {
                if (userNetwork.getNetworkId().getDescription().equals(network.getDescription())) {
                    dbUserNetwork = userNetwork;
                    break;
                }
            }
            if (dbUserNetwork != null) {
                if (network.getProfilePicture() != null && network.getProfilePicture().length() > 0) {
                    dbUserNetwork.setProfilePicture(network.getProfilePicture());  
                }
                dbUserNetwork.setUpdatedAt(new Date());
                dbUserNetwork.setUpdatedBy(1);                                 
            }    
        }        

        // Misc user object info
        dbUser.setUpdatedAt(new Date());
        dbUser.setUpdatedBy(1);
        // Save the new user
        try {
            dbUser = uf.save(dbUser);
        } catch(Exception e) {
            throw new WebApplicationException(new Throwable("Database error. Message: " + e.getMessage()), Response.Status.INTERNAL_SERVER_ERROR);
        }
        return Response.noContent().build();          
    }    
}
